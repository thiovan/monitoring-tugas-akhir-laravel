@extends('adminlte::login')
@section('js')
<script>
	$('#btn_login').click(function(){
		sendPost();
	});

	function sendPost() {
		$('#message_box').hide();
		$.ajax({
			type: "POST",
			url: '/api/user/login',
			data: $("#login_form").serialize(),
			success:function(data){
				console.log(data);
				if (data.status == true) {
					if(data.data.tipe == 'Admin'){
						localStorage.setItem('NomorInduk',data.data.id);
						window.location.replace('/dashboard');
					}else{
						$('#message_box').show();
						$('#message').text('Hanya admin yang diperbolehkan mengakses halaman dashboard');
					}
				}else{
					$('#message_box').show();
					$('#message').text(data.message);
				}
			},
			error:function(){
				console.log('error');
			}
		});
	}
</script>
@stop