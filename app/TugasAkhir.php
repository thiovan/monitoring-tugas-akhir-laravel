<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TugasAkhir extends Model
{
	public function penyusun1Attribute()
	{
		return $this->belongsTo('App\User', 'penyusun_1');
	}

	public function penyusun2Attribute()
	{
		return $this->belongsTo('App\User', 'penyusun_2');
	}

	public function penyusun3Attribute()
	{
		return $this->belongsTo('App\User', 'penyusun_3');
	}

	public function pembimbing1Attribute()
	{
		return $this->belongsTo('App\User', 'pembimbing_1');
	}

	public function pembimbing2Attribute()
	{
		return $this->belongsTo('App\User', 'pembimbing_2');
	}
}
