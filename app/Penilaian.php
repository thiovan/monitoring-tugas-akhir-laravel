<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    public function mahasiswaAttribute()
    {
    	return $this->belongsTo('App\User', 'mahasiswa');
    }

    public function penilaiAttribute()
    {
    	return $this->belongsTo('App\User', 'penilai');
    }

}
