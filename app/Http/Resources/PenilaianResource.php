<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PenilaianResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        'id' => $this->id,
        'mahasiswa' => $this->mahasiswaAttribute,
        'penilai' => $this->penilaiAttribute,
        'sebagai' => $this->sebagai,
        'nilai_1' => $this->nilai_1,
        'nilai_2' => $this->nilai_2,
        'nilai_3' => $this->nilai_3,
        'nilai_4' => $this->nilai_4,
        ];
    }
}
