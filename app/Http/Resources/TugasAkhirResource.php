<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TugasAkhirResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        'id'            => $this->id,
        'judul'         => $this->judul,
        'penyusun_1'    => $this->penyusun1Attribute,
        'penyusun_2'    => $this->penyusun2Attribute,
        'penyusun_3'    => $this->penyusun3Attribute,
        'pembimbing_1'  => $this->pembimbing1Attribute,
        'pembimbing_2'  => $this->pembimbing2Attribute,
        'proposal'      => $this->proposal,
        'status'        => $this->status,
        'selesai'       => $this->selesai,
        'created_at'    => (String) $this->created_at,
        'updated_at'    => (String) $this->updated_at,
        ];
    }
}
