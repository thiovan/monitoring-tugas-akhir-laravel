<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\TugasAkhir;
use App\User;

class HasilResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
     return [
        'id' => $this->id,
        'tugas_akhir' => new TugasAkhirResource(TugasAkhir::find($this->tugas_akhir)),
        'mahasiswa' => new UserResource(User::find($this->mahasiswa)),
        'rata2_pembimbing' => $this->rata2_pembimbing,
        'rata2_penguji' => $this->rata2_penguji,
        'hasil' => $this->hasil,
    ];
}
}
