<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\TugasAkhir;
use App\User;

class SidangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        'id'            => $this->id,
        'tugas_akhir'   => new TugasAkhirResource(TugasAkhir::find($this->tugas_akhir)),
        'tanggal'       => $this->tanggal,
        'jam'           => $this->jam,
        'ruang'         => $this->ruang,
        'sekretaris'    => new UserResource(User::find($this->sekretaris)),
        'penguji_1'     => new UserResource(User::find($this->penguji_1)),
        'penguji_2'     => new UserResource(User::find($this->penguji_2)),
        'penguji_3'     => new UserResource(User::find($this->penguji_3)),
        'status'        => $this->status,
        'selesai'       => $this->selesai,
        ];
    }
}
