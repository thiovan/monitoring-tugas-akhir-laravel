<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\TugasAkhir;

class BimbinganResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        'id'            => $this->id,
        'mahasiswa'     => $this->mahasiswaAttribute,
        'tugas_akhir'   => new TugasAkhirResource(TugasAkhir::find($this->tugas_akhir)),
        'pembimbing'    => $this->pembimbingAttribute,
        'keterangan'    => $this->keterangan,
        'status'        => $this->status,
        'created_at'    => (string) $this->created_at,
        'updated_at'    => (string) $this->updated_at,
        ];
    }
}
