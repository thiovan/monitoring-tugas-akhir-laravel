<?php

namespace App\Http\Controllers;

use App\Hasil;
use App\TugasAkhir;
use App\Sidang;
use App\Http\Resources\HasilResource;
use Illuminate\Http\Request;

class HasilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $Hasil = new Hasil;
            $Hasil->tugas_akhir = $request->tugas_akhir;
            $Hasil->mahasiswa = $request->mahasiswa;
            $Hasil->rata2_pembimbing = $request->rata2_pembimbing;
            $Hasil->rata2_penguji = $request->rata2_penguji;
            $Hasil->hasil = $request->hasil;
            $Hasil->save();

            $penilaian_selesai = false;
            $TugasAkhir = TugasAkhir::find($request->tugas_akhir);

            $penyusun_1 = Hasil::where('mahasiswa', $TugasAkhir->penyusun_1)->count();
            $penyusun_2 = Hasil::where('mahasiswa', $TugasAkhir->penyusun_2)->count();
            $penyusun_3 = Hasil::where('mahasiswa', $TugasAkhir->penyusun_3)->count();

            if ($TugasAkhir->penyusun_2 == null) {

                if ($penyusun_1 > 0) {
                    $penilaian_selesai = true;
                }

            } else if ($TugasAkhir->penyusun_3 == null) {

                if ($penyusun_1 > 0 && $penyusun_2 > 0) {
                    $penilaian_selesai = true;
                }

            } else {

                if ($penyusun_1 > 0 && $penyusun_2 > 0 && $penyusun_3 > 0) {
                    $penilaian_selesai = true;
                }

            }

            if ($penilaian_selesai) {

                $TugasAkhir->status = '2';
                $TugasAkhir->save();
                $Sidang = Sidang::where('tugas_akhir', $TugasAkhir->id)->first();
                $Sidang->status = '2';
                $Sidang->save();

            }

            return response()->json([
                'status' => TRUE,
                'message' => "Data Berhasil Disimpan",
                'data' => new HasilResource($Hasil)
            ]);

            
        } catch (\Exception $e) {

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Menyimpan Data",
                'data' => $e->getMessage()

            ]); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hasil  $hasil
     * @return \Illuminate\Http\Response
     */
    public function show(Hasil $hasil)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hasil  $hasil
     * @return \Illuminate\Http\Response
     */
    public function edit(Hasil $hasil)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hasil  $hasil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hasil $hasil)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hasil  $hasil
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hasil $hasil)
    {
        //
    }

    public function getHasilByMahasiswa($id_mahasiswa)
    {
        try {

            $Hasil = Hasil::where('mahasiswa', $id_mahasiswa)->first();

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => new HasilResource($Hasil)
            ]);

        } catch (\Exception $e) {

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]); 

        }
    }
}
