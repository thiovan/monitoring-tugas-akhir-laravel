<?php

namespace App\Http\Controllers;

use App\Bimbingan;
use App\TugasAkhir;
use App\User;
use App\Http\Resources\BimbinganResource;
use Illuminate\Http\Request;

class BimbinganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $Bimbingan = new Bimbingan;
            $Bimbingan->mahasiswa = $request->mahasiswa;

            $TugasAkhir = TugasAkhir::where('penyusun_1' ,$request->mahasiswa)
            ->orWhere('penyusun_2', $request->mahasiswa)
            ->orWhere('penyusun_3', $request->mahasiswa)
            ->first();

            $Bimbingan->tugas_akhir = $TugasAkhir->id;
            $Bimbingan->pembimbing = $request->pembimbing;
            $Bimbingan->keterangan = $request->keterangan;
            $Bimbingan->status = '0';
            $Bimbingan->save();

            $Bimbingan = Bimbingan::find($Bimbingan->id);

            include('Functions\FirebaseNotification.php');
            $title = 'Konfirmasi Bimbingan';
            $inner_title = 'Permintaan Konfirmasi Bimbingan Baru';
            $message = 'Judul TA: ' . $TugasAkhir->judul;
            sendNotification($title, $inner_title, $message, 'monta_' . $Bimbingan->pembimbing);
            
            return response()->json([
                'status' => TRUE,
                'message' => "Data Berhasil Disimpan",
                'data' => new BimbinganResource($Bimbingan)
            ]); 

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Data Gagal Disimpan",
                'data' => $e->getMessage()
            ]);  

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bimbingan  $bimbingan
     * @return \Illuminate\Http\Response
     */
    public function show(Bimbingan $bimbingan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bimbingan  $bimbingan
     * @return \Illuminate\Http\Response
     */
    public function edit(Bimbingan $bimbingan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bimbingan  $bimbingan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $Bimbingan = Bimbingan::find($id);
            $Bimbingan->tugas_akhir = $request->tugas_akhir;
            $Bimbingan->pembimbing = $request->pembimbing;
            $Bimbingan->keterangan = $request->keterangan;
            $Bimbingan->status = $request->status;
            $Bimbingan->save();

            $Bimbingan = Bimbingan::find($Bimbingan->id);

            return response()->json([
                'status' => TRUE,
                'message' => "Data Berhasil Diubah",
                'data' => new BimbinganResource($Bimbingan)
            ]); 

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Data Gagal Diubah",
                'data' => $e->getMessage()
            ]);  

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bimbingan  $bimbingan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bimbingan $bimbingan)
    {
        //
    }

    public function bimbinganByMahasiswa($id)
    {
        try{

            $Bimbingans = Bimbingan::where('mahasiswa', $id)->orderBy('created_at', 'ASC')->get();

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => BimbinganResource::collection($Bimbingans)
            ]);

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]);

        }
    }

    public function cekStatusBimbingan($id)
    {
        try{

            $TugasAkhir = TugasAkhir::where('penyusun_1', $id)
            ->orWhere('penyusun_2', $id)
            ->orWhere('penyusun_3', $id)
            ->first();

            $Bimbingan1 = Bimbingan::where('mahasiswa', $id)
            ->where('pembimbing', $TugasAkhir->pembimbing_1)
            ->where('status', '1')
            ->count();

            $Bimbingan2 = Bimbingan::where('mahasiswa', $id)
            ->where('pembimbing', $TugasAkhir->pembimbing_2)
            ->where('status', '1')
            ->count();

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => ['total_bimbingan_1' => $Bimbingan1, 'total_bimbingan_2' => $Bimbingan2]
            ]);

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => $e->getMessage(),
                'data' => null
            ], 500);

        }
    }

    public function bimbinganByTA($id_ta, $id_pembimbing)
    {
        try{

            $Bimbingans = Bimbingan::where('tugas_akhir', $id_ta)
            ->where('pembimbing', $id_pembimbing)
            ->orderBy('created_at', 'ASC')
            ->get();

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => BimbinganResource::collection($Bimbingans)
            ]);

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]);

        }
    }

    public function updateStatusBimbingan(Request $request, $id)
    {
        try {

            $Bimbingan = Bimbingan::find($id);
            $Bimbingan->status = $request->status;
            $Bimbingan->save();

            $TugasAkhir = TugasAkhir::find($Bimbingan->tugas_akhir);
            include('Functions\FirebaseNotification.php');
            $title = 'Konfirmasi Bimbingan';
            $inner_title = 'Bimbingan Anda Telah Dikonfirmasi';
            $message = 'Judul TA: ' . $TugasAkhir->judul;
            sendNotification($title, $inner_title, $message, 'monta_' . $Bimbingan->mahasiswa);

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => new BimbinganResource($Bimbingan)
            ]);

        } catch (\Exception $e) {

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]);

        }
    }

}
