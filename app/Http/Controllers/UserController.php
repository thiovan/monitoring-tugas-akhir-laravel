<?php

namespace App\Http\Controllers;

use App\User;
use App\TugasAkhir;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;

class UserController extends Controller
{

	//login pengguna
	public function login(Request $request)
	{
		$nomor_induk = $request->nomor_induk;
		$password = $request->password;

		//Check if nomor_induk exist
		$User = User::where('nomor_induk', '=', $nomor_induk)->first();
		if ($User === NULL) {

			//return false, nomor_induk not exist
			return response()->json([
				'status' => FALSE,
				'message' => 'Nomor Induk Tidak Terdaftar',
				'data' => NULL
			]);

		}else{

			//check if password match
			if (decrypt($User->password) === $password) {

				//return true, login success
				return response()->json([
					'status' => TRUE,
					'message' => 'Login Berhasil',
					'data' => new UserResource($User)
				]);

			}else{

				//return false, password not match
				return response()->json([
					'status' => FALSE,
					'message' => 'Password Salah',
					'data' => NULL
				]);

			}
		}
	}

	//ubah data pengguna
	public function update(Request $request, $id)
	{
		try{
			$User = User::find($id);
			$User->nomor_induk = $request->nomor_induk;
			$User->nama = $request->nama;
			$User->kelas = $request->kelas;
			$User->tipe = $request->tipe;

			if ($User->password != $request->password) {
				$User->password = encrypt($request->password);
			}

			if ($request->hasFile('foto')) {
				if (File::exists('foto/'.$User->foto)) {
					File::delete('foto/'.$User->foto);
				}
				$foto = time().'.'.$request->foto->getClientOriginalExtension();
				$request->foto->move(public_path('foto'), $foto);
				$User->foto = $foto;
			}

			$User->save();

			$User->foto = url('foto/' . $User->foto);
			return response()->json([
				'status' => TRUE,
				'message' => 'Data User Berhasil Diubah',
				'data' => $User
			]);

		}catch(\Exception $e){

			return response()->json([
				'status' => FALSE,
				'message' => 'Data User Gagal Diubah',
				'data' => $e->getMessage()
			]);
		}
	}

	//ambil daftar pembimbing
	public function pembimbingList()
	{
		try{

			$Users = User::where('pembimbing', '1')->get();

			return response()->json([
				'status' => TRUE,
				'message' => "Berhasil Mengambil Data",
				'data' => UserResource::collection($Users)
			]);

		}catch(\Exception $e){

			return response()->json([
				'status' => FALSE,
				'message' => "Gagal Mengambil Data",
				'data' => $e->getMessage()
			]);

		}
	}

	//ambil daftar mahasiswa
	public function mahasiswaList()
	{
		try{

			$Users = User::where('tipe', 'Mahasiswa')->get();

			return response()->json([
				'status' => TRUE,
				'message' => "Berhasil Mengambil Data",
				'data' => UserResource::collection($Users)
			]);

		}catch(\Exception $e){

			return response()->json([
				'status' => FALSE,
				'message' => "Gagal Mengambil Data",
				'data' => $e->getMessage()
			]);

		}
	}

	//ambil daftar pembimbing sesuai id mahasiswa
	public function pembimbingListByMahasiswa($id_mahasiswa)
	{
		try{

			$TugasAkhir = TugasAkhir::where('penyusun_1', $id_mahasiswa)
			->orWhere('penyusun_2', $id_mahasiswa)
			->orWhere('penyusun_3', $id_mahasiswa)
			->first();

			$Users = User::where('pembimbing', '1')
			->whereIn('id', array($TugasAkhir->pembimbing_1, $TugasAkhir->pembimbing_2))
			->get();

			return response()->json([
				'status' => TRUE,
				'message' => "Berhasil Mengambil Data",
				'data' => UserResource::collection($Users)
			]);

		}catch(\Exception $e){

			return response()->json([
				'status' => FALSE,
				'message' => "Gagal Mengambil Data",
				'data' => $e->getMessage()
			]);

		}
	}
}
