<?php

namespace App\Http\Controllers;

use App\TugasAkhir;
use App\User;
use App\Http\Resources\TugasAkhirResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;

class TugasAkhirController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{

            $TugasAkhirs = TugasAkhir::all();

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => TugasAkhirResource::collection($TugasAkhirs)
            ]);

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $TugasAkhir = new TugasAkhir;
            $TugasAkhir->judul = $request->judul;
            $TugasAkhir->penyusun_1 = $request->penyusun_1;
            $TugasAkhir->penyusun_2 = $request->penyusun_2;
            $TugasAkhir->penyusun_3 = $request->penyusun_3;
            $TugasAkhir->pembimbing_1 = $request->pembimbing_1;
            $TugasAkhir->pembimbing_2 = $request->pembimbing_2;
            $TugasAkhir->status = $request->status;

            if ($request->hasFile('proposal')) {
                if (File::exists('proposal/'.$request->proposal)) {
                    File::delete('proposal/'.$request->proposal);
                }
                $proposal = time().'.'.$request->proposal->getClientOriginalExtension();
                $request->proposal->move(public_path('proposal'), $proposal);
                $TugasAkhir->proposal = $proposal;
            }

            $TugasAkhir->save();

            include('Functions\FirebaseNotification.php');
            $title = 'Pendaftaran Tugas Akhir';
            $inner_title = 'Permintaan Konfirmasi Tugas Akhir Baru';
            $message = 'Judul TA: ' . $TugasAkhir->judul;
            sendNotification($title, $inner_title, $message, 'monta_admin');

            return response()->json([
                'status' => TRUE,
                'message' => "Data Berhasil Disimpan",
                'data' => new TugasAkhirResource($TugasAkhir)
            ]); 

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Data Gagal Disimpan",
                'data' => $e->getMessage()
            ]);  

        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\TugasAkhir  $tugasAkhir
     * @return \Illuminate\Http\Response
     */
    public function show(TugasAkhir $tugasAkhir)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TugasAkhir  $tugasAkhir
     * @return \Illuminate\Http\Response
     */
    public function edit(TugasAkhir $tugasAkhir)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TugasAkhir  $tugasAkhir
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $TugasAkhir = TugasAkhir::find($id);
            $TugasAkhir->pembimbing_1 = $request->pembimbing_1;
            $TugasAkhir->pembimbing_2 = $request->pembimbing_2;
            $TugasAkhir->status = $request->status;
            $TugasAkhir->save();

            include('Functions\FirebaseNotification.php');
            $title = 'Pendaftaran Tugas Akhir';
            $inner_title = 'Pendaftaran Tugas Akhir Anda Telah Dikonfirmasi';
            $message = 'Mohon untuk segera menemui pembimbing untuk melakukan bimbingan';
            sendNotification($title, $inner_title, $message, 'monta_' . $TugasAkhir->penyusun_1);
            if ($TugasAkhir->penyusun_2 != null) {
                sendNotification($title, $inner_title, $message, 'monta_' . $TugasAkhir->penyusun_2);
            }
            if ($TugasAkhir->penyusun_3 != null) {
                sendNotification($title, $inner_title, $message, 'monta_' . $TugasAkhir->penyusun_3);
            }

            return response()->json([
                'status' => TRUE,
                'message' => "Data Berhasil Diubah",
                'data' => new TugasAkhirResource($TugasAkhir)
            ]); 

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Data Gagal Diubah",
                'data' => $e->getMessage()
            ]);  

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TugasAkhir  $tugasAkhir
     * @return \Illuminate\Http\Response
     */
    public function destroy(TugasAkhir $tugasAkhir)
    {
        //
    }

    public function cekStatusTA($id_user)
    {
        try{

            $TugasAkhir = TugasAkhir::where('penyusun_1', $id_user)
            ->orWhere('penyusun_2', $id_user)
            ->orWhere('penyusun_3', $id_user)
            ->first();

            if (@count($TugasAkhir)) {

                return response()->json([
                    'status' => TRUE,
                    'message' => "Berhasil Mengambil Data",
                    'data' => new TugasAkhirResource($TugasAkhir)
                ]);
            }else{

                return response()->json([
                    'status' => TRUE,
                    'message' => "Berhasil Mengambil Data",
                    'data' => null
                ]);
            }

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]);

        }
    }

    public function konfirmasiTugasAkhirList()
    {
        try{

            $TugasAkhirs = TugasAkhir::where('status', '0')->get();

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => TugasAkhirResource::collection($TugasAkhirs)
            ]);

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]);

        }
    }

    public function bimbinganTugasAkhirList($id_dosen)
    {
        try {

            $TugasAkhirs = TugasAkhir::where('pembimbing_1', $id_dosen)
            ->where('status', '!=', '2')
            ->orWhere('pembimbing_2', $id_dosen)
            ->get();

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => TugasAkhirResource::collection($TugasAkhirs)
            ]);

        } catch (\Exception $e) {

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]);

        }
    }

}
