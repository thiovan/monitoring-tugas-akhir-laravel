<?php

namespace App\Http\Controllers;

use App\Penilaian;
use App\User;
use App\Http\Resources\PenilaianResource;
use Illuminate\Http\Request;

class PenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $Penilaian = new Penilaian;
            $Penilaian->mahasiswa = $request->mahasiswa;
            $Penilaian->penilai = $request->penilai;
            $Penilaian->sebagai = $request->sebagai;
            $Penilaian->nilai_1 = $request->nilai_1;
            $Penilaian->nilai_2 = $request->nilai_2;
            $Penilaian->nilai_3 = $request->nilai_3;
            $Penilaian->nilai_4 = $request->nilai_4;
            $Penilaian->save();

            $penilaian = Penilaian::find($Penilaian->id);

            return response()->json([
                'status' => TRUE,
                'message' => "Data Berhasil Disimpan",
                'data' => new PenilaianResource($Penilaian)
            ]);
        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Data Gagal Disimpan",
                'data' => $e->getMessage()
            ]); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Penilaian  $penilaian
     * @return \Illuminate\Http\Response
     */
    public function show($id_mahasiswa, $id_penilai)
    {
        try{

            $Penilaian = Penilaian::where('mahasiswa', $id_mahasiswa)
            ->where('penilai', $id_penilai)
            ->first();

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => new PenilaianResource($Penilaian)
            ]);

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Penilaian  $penilaian
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penilaian  $penilaian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penilaian $penilaian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penilaian  $penilaian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penilaian $penilaian)
    {
        //
    }

    public function showPenilaianMahasiswa($id_mahasiswa)
    {
        try{

            $Penilaians = Penilaian::where('mahasiswa', $id_mahasiswa)->get();

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => PenilaianResource::collection($Penilaians)
            ]);

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
            ]);
        }
    }

}
