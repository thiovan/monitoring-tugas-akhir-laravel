<?php

namespace App\Http\Controllers;

use App\Sidang;
use App\TugasAkhir;
use App\User;
use App\Http\Resources\SidangResource;
use Illuminate\Http\Request;

class SidangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $TugasAkhir = TugasAkhir::where('penyusun_1', $request->mahasiswa)
            ->orWhere('penyusun_2', $request->mahasiswa)
            ->orWhere('penyusun_3', $request->mahasiswa)
            ->first();

            $Sidang = new Sidang;
            $Sidang->tugas_akhir = $TugasAkhir->id;
            $Sidang->status = '0';
            $Sidang->save();

            $Sidang = Sidang::find($Sidang->id);

            $TugasAkhir = TugasAkhir::find($Sidang->tugas_akhir);
            include('Functions\FirebaseNotification.php');
            $title = 'Pendaftaran Sidang';
            $inner_title = 'Permintaan Konfirmasi Sidang Baru';
            $message = 'Judul TA: ' . $TugasAkhir->judul;
            sendNotification($title, $inner_title, $message, 'monta_admin');

            return response()->json([
                'status' => TRUE,
                'message' => "Data Berhasil Disimpan",
                'data' => new SidangResource($Sidang)
                ]);

        }catch(\Exception $e){
            return response()->json([
                'status' => FALSE,
                'message' => "Data Gagal Disimpan",
                'data' => $e->getMessage()
                ]); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sidang  $sidang
     * @return \Illuminate\Http\Response
     */
    public function show(Sidang $sidang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sidang  $sidang
     * @return \Illuminate\Http\Response
     */
    public function edit(Sidang $sidang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sidang  $sidang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $Sidang = Sidang::find($id);
            $Sidang->tanggal = $request->tanggal;
            $Sidang->jam = $request->jam;
            $Sidang->ruang = $request->ruang;
            $Sidang->sekretaris = $request->sekretaris;
            $Sidang->penguji_1 = $request->penguji_1;
            $Sidang->penguji_2 = $request->penguji_2;
            $Sidang->penguji_3 = $request->penguji_3;
            $Sidang->status = $request->status;
            $Sidang->save();

            $Sidang = Sidang::find($Sidang->id);

            $TugasAkhir = TugasAkhir::find($Sidang->tugas_akhir);
            include('Functions\FirebaseNotification.php');
            $title = 'Pendaftaran Sidang';
            $inner_title = 'Pendaftaran Sidang Anda Telah Dikonfirmasi';
            $message = 'Mohon untuk segera mempersiapkan segala keperluan sidang';
            sendNotification($title, $inner_title, $message, 'monta_' . $TugasAkhir->penyusun_1);
            if ($TugasAkhir->penyusun_2 != null) {
                sendNotification($title, $inner_title, $message, 'monta_' . $TugasAkhir->penyusun_2);
            }
            if ($TugasAkhir->penyusun_3 != null) {
                sendNotification($title, $inner_title, $message, 'monta_' . $TugasAkhir->penyusun_3);
            }

            return response()->json([
                'status' => TRUE,
                'message' => "Data Berhasil Diubah",
                'data' => new SidangResource($Sidang)
                ]);
        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Data Gagal Diubah",
                'data' => $e->getMessage()
                ]);  
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sidang  $sidang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sidang $sidang)
    {
        //
    }

    public function cekStatusSidang($id_user)
    {
        try{

            $TugasAkhir = TugasAkhir::where('penyusun_1', $id_user)
            ->orWhere('penyusun_2', $id_user)
            ->orWhere('penyusun_3', $id_user)
            ->first();

            $Sidang = Sidang::where('tugas_akhir', $TugasAkhir->id)->first();

            if (!empty($Sidang)) {

                $Sidang = Sidang::where('tugas_akhir', $TugasAkhir->id)->first();
                return response()->json([
                    'status' => TRUE,
                    'message' => "Berhasil Mengambil Data",
                    'data' => new SidangResource($Sidang)
                    ]);
            }else{

                return response()->json([
                    'status' => TRUE,
                    'message' => "Berhasil Mengambil Data",
                    'data' => null
                    ]);
            }

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
                ]);

        }
    }

    public function daftarKonfirmasiSidang()
    {
        try{
            $Sidangs = Sidang::where('status', '0')->get();
            
            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => SidangResource::collection($Sidangs)
                ]);

        }catch(\Exception $e){
            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
                ]);
        }
    }

    public function daftarSidangByDosen($id_user)
    {
        try{

            $TugasAkhir = TugasAkhir::where('pembimbing_1', $id_user)
            ->where('status', '!=', '2')
            ->orWhere('pembimbing_2', $id_user)
            ->first();

            if (@count($TugasAkhir)) {
                $Sidangs = Sidang::where('sekretaris', $id_user)
                ->where('status', '!=', '2')
                ->orWhere('penguji_1', $id_user)
                ->orWhere('penguji_2', $id_user)
                ->orWhere('penguji_3', $id_user)
                ->orWhere('tugas_akhir', $TugasAkhir->id)
                ->get();
            }else{
                $Sidangs = Sidang::where('sekretaris', $id_user)
                ->where('status', '!=', '2')
                ->orWhere('penguji_1', $id_user)
                ->orWhere('penguji_2', $id_user)
                ->orWhere('penguji_3', $id_user)
                ->get();
            }

            return response()->json([
                'status' => TRUE,
                'message' => "Berhasil Mengambil Data",
                'data' => SidangResource::collection($Sidangs)
                ]);

        }catch(\Exception $e){

            return response()->json([
                'status' => FALSE,
                'message' => "Gagal Mengambil Data",
                'data' => $e->getMessage()
                ]);
        } 
    }
}
