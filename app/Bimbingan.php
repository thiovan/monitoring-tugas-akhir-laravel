<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bimbingan extends Model
{
	public function mahasiswaAttribute()
	{
		return $this->belongsTo('App\User', 'mahasiswa');
	}

	public function pembimbingAttribute()
	{
		return $this->belongsTo('App\User', 'pembimbing');
	}
}
