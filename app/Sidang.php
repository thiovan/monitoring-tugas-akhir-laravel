<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sidang extends Model
{
	public function sekretarisAttribute()
	{
		return $this->belongsTo('App\User', 'sekretaris');
	}

	public function penguji1Attribute()
	{
		return $this->belongsTo('App\User', 'penguji_1');
	}

	public function penguji2Attribute()
	{
		return $this->belongsTo('App\User', 'penguji_2');
	}

	public function penguji3Attribute()
	{
		return $this->belongsTo('App\User', 'penguji_3');
	}
}
