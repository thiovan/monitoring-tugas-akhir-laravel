<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//User
Route::post('user/login', 'UserController@login');
Route::post('user/{id}', 'UserController@update');
Route::get('user/mahasiswa', 'UserController@mahasiswaList');
Route::get('user/pembimbing', 'UserController@pembimbingList');
Route::get('user/pembimbing/{id}', 'UserController@pembimbingListByMahasiswa');

//Tugas Akhir
Route::post('ta', 'TugasAkhirController@store');
Route::post('ta/{id}', 'TugasAkhirController@update');
Route::get('ta', 'TugasAkhirController@index');
Route::get('ta/cek/{id_user}', 'TugasAkhirController@cekStatusTA');
Route::get('ta/konfirmasi', 'TugasAkhirController@konfirmasiTugasAkhirList');
Route::get('ta/bimbingan/{id_dosen}', 'TugasAkhirController@bimbinganTugasAkhirList');

//Bimbingan
Route::post('bimbingan', 'BimbinganController@store');
Route::post('bimbingan/status/{id}', 'BimbinganController@updateStatusBimbingan');
Route::get('bimbingan/mahasiswa/{id}', 'BimbinganController@bimbinganByMahasiswa');
Route::get('bimbingan/ta/{id_ta}/{id_pembimbing}', 'BimbinganController@bimbinganByTA');
Route::get('bimbingan/cek/{id}', 'BimbinganController@cekStatusBimbingan');

//Sidang
Route::post('sidang', 'SidangController@store');
Route::post('sidang/{id}', 'SidangController@update');
Route::get('sidang/cek/{id_user}', 'SidangController@cekStatusSidang');
Route::get('sidang/konfirmasi', 'SidangController@daftarKonfirmasiSidang');
Route::get('sidang/dosen/{id_user}', 'SidangController@daftarSidangByDosen');

//Penilaian
Route::post('penilaian', 'PenilaianController@store');
Route::get('penilaian/show/{id_mahasiswa}/{id_penilai}', 'PenilaianController@show');
Route::get('penilaian/show/{id_mahasiswa}', 'PenilaianController@showPenilaianMahasiswa');

//Hasil
Route::post('hasil', 'HasilController@store');
Route::get('hasil/{id_mahasiswa}', 'HasilController@getHasilByMahasiswa');

