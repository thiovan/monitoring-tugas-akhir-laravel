<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_induk')->unique();
            $table->string('nama');
            $table->text('password');
            $table->string('kelas');
            $table->string('tipe');
            $table->string('foto');
            $table->boolean('pembimbing');
            $table->boolean('penguji');
            $table->boolean('sekretaris');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
