<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\TugasAkhir::class, function (Faker $faker) {
	static $i = 1;
    $judul = array('Sistem Pemonitoran Dan Pengendalian Katup Pada Filtrasi Air Tanah Berbasis Android', 'Mobile Application Sistem Monitoring Kondisi Pasien Serangan Jantung Berbasis Google Maps Dan Android');
    $penyusun_2 = array('3', null);
	$penyusun_3 = array(null, null);
	$pembimbing_1 = array('4', '5');
	$pembimbing_2 = array('5', '6');
    $status = array('1', '0');
	
    return [
        'penyusun_1' => $i++,
        'judul' => $judul[$i-2],
        'penyusun_2' => $penyusun_2[$i-2],
        'penyusun_3' => $penyusun_3[$i-2],
        'pembimbing_1' => $pembimbing_1[$i-2],
        'pembimbing_2' => $pembimbing_2[$i-2],
        'status' => $status[$i-2],
    ];
});
