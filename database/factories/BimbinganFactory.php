<?php

use Faker\Generator as Faker;

$factory->define(App\Bimbingan::class, function (Faker $faker) {

    return [
        'mahasiswa' => '1',
        'tugas_akhir' => '1',
        'pembimbing' => '4',
        'keterangan' => 'Ini keterangan',
        'status' => '0',
    ];
});
