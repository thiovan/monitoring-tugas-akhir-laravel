<?php

use Faker\Generator as Faker;
use App\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(App\User::class, function (Faker $faker) {

	static $i = 1;
	$kelas = array('IK-3A', 'IK-3A', 'IK-3B', '', '', '', '', '', '', '');
	$tipe = array('Mahasiswa', 'Mahasiswa', 'Mahasiswa', 'Dosen', 'Dosen', 'Dosen', 'Dosen', 'Dosen', 'Dosen', 'Admin');
	$pembimbing = array(0, 0, 0, 1, 1, 1, 1, 1, 1, 1);
	$penguji = array(0, 0, 0, 1, 1, 1, 1, 1, 1, 1);
	$sekretaris = array(0, 0, 0, 1, 1, 1, 1, 1, 1, 1);

	return [
		'nomor_induk' => $i++,
		'nama' => $faker->name,
		'password' => encrypt("pass"),
		'kelas' => $kelas[$i-2],
		'tipe' => $tipe[$i-2],
		'foto' => 'default.png',
		'pembimbing' => $pembimbing[$i-2],
		'penguji' => $penguji[$i-2],
		'sekretaris' => $sekretaris[$i-2],
	];
});

function autoIncrement()
{
	$i++;
}
