<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//$users = factory(App\User::class, 10)->create();
    	$kelas = array('IK-3A', 'IK-3A', 'IK-3A', 'IK-3B', 'IK-3B', '', '', '', '', '', '', '');
    	$tipe = array('Mahasiswa', 'Mahasiswa', 'Mahasiswa', 'Mahasiswa', 'Mahasiswa', 'Dosen', 'Dosen', 'Dosen', 'Dosen', 'Dosen', 'Dosen', 'Admin');
    	$pembimbing = array(0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1);
    	$penguji = array(0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1);
    	$sekretaris = array(0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1);

    	$faker = Faker::create();
    	for ($i=0; $i < 12; $i++) { 
    		$User = new User;
    		$User->nomor_induk = $i+1;
    		$User->nama = $i+1 . $faker->name;
    		$User->password = encrypt('pass');
    		$User->kelas = $kelas[$i];
    		$User->tipe = $tipe[$i];
    		$User->foto = 'default.png';
    		$User->pembimbing = $pembimbing[$i];
    		$User->penguji = $penguji[$i];
    		$User->sekretaris = $sekretaris[$i];
    		$User->save();
    	}
    }
}
