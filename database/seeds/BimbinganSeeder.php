<?php

use Illuminate\Database\Seeder;
use App\Bimbingan;

class BimbinganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$bimbingan = factory(App\Bimbingan::class, 5)->create();

    	for ($i=0; $i < 7; $i++) { 
    		$Bimbingan = new Bimbingan;
    		$Bimbingan->mahasiswa = '1';
    		$Bimbingan->tugas_akhir = '1';
    		$Bimbingan->pembimbing = '5';
    		$Bimbingan->keterangan = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in ultrices lacus, in tempor lorem.';
    		$Bimbingan->status = '1';
    		$Bimbingan->save();
    	}
    	for ($i=0; $i < 8; $i++) { 
    		$Bimbingan = new Bimbingan;
    		$Bimbingan->mahasiswa = '1';
    		$Bimbingan->tugas_akhir = '1';
    		$Bimbingan->pembimbing = '6';
    		$Bimbingan->keterangan = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in ultrices lacus, in tempor lorem. ';
    		$Bimbingan->status = '1';
    		$Bimbingan->save();
    	}

    	$Bimbingan = new Bimbingan;
    	$Bimbingan->mahasiswa = '1';
    	$Bimbingan->tugas_akhir = '1';
    	$Bimbingan->pembimbing = '5';
    	$Bimbingan->keterangan = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in ultrices lacus, in tempor lorem. ';
    	$Bimbingan->status = '0';
    	$Bimbingan->save();
    	
    }
}
